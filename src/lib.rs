pub struct Game {
    rolls: [i32; 21],
    curr_roll: usize
}

impl Game {

    fn is_spare(&self, roll: usize) -> bool {
        self.rolls[roll] + self.rolls[roll+1] == 10
    }

    fn is_strike(&self, roll: usize) -> bool {
        self.rolls[roll] == 10
    }

    pub fn new() -> Self {
        Self {
            rolls: [0; 21],
            curr_roll: 0
        }
    }

    pub fn roll(&mut self, pins: i32) {
        self.rolls[self.curr_roll] = pins;
        self.curr_roll += 1
    }
    
    pub fn score(&self) -> i32 {
        let mut score = 0;
        let mut first_roll_in_frame = 0;
        for _frame in 1..=10 {
            if self.is_strike(first_roll_in_frame) {
                score += 10 + self.rolls[first_roll_in_frame+1] + self.rolls[first_roll_in_frame+2];
                first_roll_in_frame += 1;
            }
            else if self.is_spare(first_roll_in_frame) {
                score += 10 + self.rolls[first_roll_in_frame+2];
                first_roll_in_frame += 2;
            }
            else {
                score += self.rolls[first_roll_in_frame] + self.rolls[first_roll_in_frame+1];
                first_roll_in_frame += 2;
            }
        }
        return score
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl Game {
        fn roll_many(&mut self, pins: i32, times: usize) {
            (0..times).for_each(|_| self.roll(pins))
        }
    }

    #[test]
    fn can_roll() {
        let mut g = Game::new();
        g.roll(0)
    }

    #[test]
    fn gutter_game() {
        let mut g = Game::new();
        g.roll_many(0, 20);
        assert_eq!(0, g.score());
    }

    #[test]
    fn all_ones() {
        let mut g = Game::new();
        g.roll_many(1, 20);
        assert_eq!(20, g.score());
    }

    #[test]
    fn one_spare() {
        let mut g = Game::new();
        g.roll(5);
        g.roll(5); // spare
        g.roll(3);
        assert_eq!(16, g.score());
    }

    #[test]
    fn one_strike() {
        let mut g = Game::new();
        g.roll(10);
        g.roll(4); 
        g.roll(3);
        assert_eq!(24, g.score());
    }

    #[test]
    fn perfect_game() {
        let mut g = Game::new();
        g.roll_many(10, 12);
        assert_eq!(300, g.score());
    }


}
